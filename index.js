// console.log("hello world!");

/*
    5.  Create a function that will accept the name of user and  will print each character of the name in reverse order. if the length of the name is more than seven characters, the consonants of the name will only be printed in console.
*/


function reverseString(userName){
    for (let i = userName.length - 1; i >= 0; i--) {
        // check if user input is > 7 
        if(userName.length > 7){
            // if user input is true then check the letter if is not equal to vowels.
            if(userName[i] !== "a" && userName[i] !== "e" && userName[i] !== "i" && userName[i] !== "o" && userName[i] !== "u"){
                // if it is equal to vowels then it will not execute the console.log
                // if it is not equal to vowels then it will execute the console.log
                console.log(userName[i]);
            }
        }
        // if user input less or equal 7 then print each letter.
        else{
            console.log(userName[i]);
        }

        
    }
}

let userName = prompt("Enter your name");
reverseString(userName);

/* ----------------------------------------------------------------- */

/*
    6.  Create an array of students with the following value
        let students = [John, Jane, Joe];
        Find the following information using array methods: mutators and accessors:
*/

let students =["John", "Jane", "Joe"];

// 1.) Total size of an array.
console.log("Total size of an array: " + students.length);

// 2.) Adding a new element at the beginning of an array. (Add Jack)

console.log("Add Jack at the beginning");   
students.unshift("jack");
console.log(students);

// 3.) Adding a new element at the end of an array. (Add Jill)

console.log("Add jill at the end");   
students.push("jill");
console.log(students);

// 4.) Removing an existing element at the beginning of an array

console.log("Remove an existing element at the beginning of an array");   
students.shift();
console.log(students);

// 5.) Removing an existing element at the end of an array.

console.log("Remove an existing element at the end of an array");   
students.pop();
console.log(students);

// 6.) Find the index number of the last element in an array.

console.log("Find the index number of the last element in an array");  
let lastIndex = students.length - 1 
console.log("Last index number: " + lastIndex);

// 7.) Print each element of the array in the browser console.
        // for each array
        // students.forEach(element => console.log(element));

console.log("Students name:")
for(i=0; i < students.length; i++){
    console.log(students[i]);
}

/* ----------------------------------------------------------------- */

/*
    7.  Using the students array, create a function that will look for the index of a given student and will remove the name of the student from the array list.
*/

function indexFinder(name){
    let index = students.findIndex(element => {
        return element.toLowerCase() === name.toLowerCase();
    });
    console.log("Index : " + index);
    students.splice(index,1);
    console.log(students);
    
}

let name = prompt("Enter student name");
indexFinder(name);

/* ----------------------------------------------------------------- */

/*
    8.  Create a "person” object that contains properties first name, last name, nickname, age, address(contains city and country), friends(with at least 3 friends with the first  name), and a function property that contains console.log() displaying text about self introduction about name, age, address and who is his/her friends.
*/

let person = {
    firstName:  "Herohito",
    lastName:  "Acuna", 
    nickName:   "Hero",
    age:    22,
    address: {
        city:   "Caloocan City",
        country: "Philippines"
    },   
    friends:["Redeemer", "Joviniel", "Jojane"]
}

function selfIntro(){
    console.log("My Name is " + person.firstName + " but you can call me " + person.nickName + ". I am " + person.age + " years old and I live in " + person.address.city + ", " + person.address.country + ". My friends are " + person.friends.join(", ") + ".");
}

selfIntro();

/* ----------------------------------------------------------------- */

/*
    Create another copy of the "person" object use in the previous activity and apply the following ES6 updates:
        1. Apply the JavaScript Destructuring Assignment in the object variable
        2. Use JavaScript Template Literals in displaying the text inside the function property of the object.
        3. Run the program and check if it still produce the expected output.
*/

// 1. Apply the JavaScript Destructuring Assignment in the object variable

// let {firstName, lastName, nickName, age, address, friends} = person;
// selfIntro();